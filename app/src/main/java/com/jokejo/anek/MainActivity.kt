package com.jokejo.anek

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.jokejo.anek.Utils.Companion.allJoke
import com.jokejo.anek.Utils.Companion.comedianList
import com.jokejo.anek.Utils.Companion.ironyList
import com.jokejo.anek.Utils.Companion.prankList
import com.jokejo.anek.Utils.Companion.satireList
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        clownClick()
        genrePrintsClick()
    }

    private fun clownClick() {
        clown.setOnClickListener {
            joke.text = allJoke.random()
            clown.animate().apply {
                duration = 45
                scaleX(0.95f)
                scaleY(0.95f)
            }.withEndAction {
                clown.animate().apply {
                    duration = 45
                    scaleX(1.00f)
                    scaleY(1.00f)
                }.start()
            }
        }
    }

    private fun genrePrintsClick() {
        cl_comedian.setOnClickListener {
            joke.text = comedianList.random()
        }
        cl_prank.setOnClickListener {
            joke.text = prankList.random()
        }
        cl_satire.setOnClickListener {
            joke.text = satireList.random()
        }
        cl_irony.setOnClickListener {
            joke.text = ironyList.random()
        }
    }
}