package com.jokejo.anek

class Utils {
    companion object{
        val satireList = listOf(
            "Jack is a rogue, a cat-Jack is a thief.\n" +
                    "Well, like the chief prosecutor",
            "What did the traffic light say to the truck? Don’t look! I’m changing!",
            "What kind of shoes do ninjas wear? Sneakers!",
            "Where do cows go for entertainment? To the moo-vies!",
            "What do you call a cow with no legs? Ground beef!"
        )
        val prankList = listOf(
            "What did Tarzan say when he saw the elephants coming? “Here come the elephants!”",
            "What can you catch but not throw? A cold!",
            "How do you know if there’s an elephant under your bed? Your head hits the ceiling!",
            "What do you call a cow with two legs? Lean meat!",
            "What animal needs to wear a wig? A bald eagle!"
        )
        val ironyList = listOf(
            " What has four wheels and flies? A garbage truck!",
            "Why are ghosts bad liars? Because you can see right through them!",
            "What do you call cheese that isn’t yours? Nacho cheese!",
            "What do you call a sleeping bull? A bulldozer!",
            "What did the zero say to the eight? Nice belt!"
        )
        val comedianList = listOf(
            "What do you call the horse that lives next door? Your neighbor!",
            "How can you tell if someone is a good farmer? He is outstanding in his field!",
            "What do cows order from? Cattle-logs!",
            "Why do sharks swim in saltwater? Because pepper water makes them sneeze!",
            "Why did the picture go to jail? It was framed!"
        )
        val allJoke = satireList + prankList + ironyList + comedianList
    }
}